# Comment lancer le projet

### Lancer le front end

```bash
cd front
npm run dev
```

### Lancer le back end

```bash
cd back
npm run dev
```
